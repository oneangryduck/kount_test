package kounttest
import (
	"fmt"
	"strings"
	"github.com/go-resty/resty"
	"time"
	"encoding/json"
)

//struct to store our results
type CountryInfo struct {
    Name   string
    Alpha2_code string
    Alpha3_code string
}
//I decided to try out custom error types
type NoCountryFoundError string

func (f NoCountryFoundError) Error() string {
	return fmt.Sprintf("No matching country found for pattern %s", string(f))
} 

type ErrorBlankLookup string

func (f ErrorBlankLookup) Error() string {
	return fmt.Sprintln("No pattern entered to search for")
} 

type HTTPServerError int

func (f HTTPServerError) Error() string {
	return fmt.Sprintf("HTTP Bad Response: Error code %d", int(f))
} 
//the main function being tested here
func GetCountryCodes(symbol string) (t time.Duration, s []CountryInfo, err error) {
	if strings.Compare(symbol, "") == 0 {
		return -1, nil, ErrorBlankLookup("")
	}
	resp, err := resty.R().Get("http://services.groupkt.com/country/search?text=" + symbol)
	if resp.StatusCode() != 200 {
		return -1, nil, HTTPServerError(resp.StatusCode())
	} 

	var countryCodes []CountryInfo //Store results here
	var jsonSample interface{}
	_ = json.Unmarshal(resp.Body(), &jsonSample)
	//if e != nil {
	//	return -1, nil, fmt.Errorf("JSON Parsing error for response body %v", resp.Body() )
	//}
	//JSON from website is somewhat messy, need to go a couple levels deep to get desired info
	m := jsonSample.(map[string]interface{})
	for k, v := range m {
		k = k
		vint := v.(map[string]interface{})
		for k2, v2 := range vint {
			if strings.Index(fmt.Sprintf("%v",v2), "No matching country") >= 0 {
				return -1, nil, NoCountryFoundError(symbol)
			}
			if strings.Compare(k2, "result") == 0 {
				//Finally the results we're after but they're formatted oddly
				var tempCode CountryInfo
				//Assertions all the way to be safe
				assertTime := v2.([]interface {})
				for _, v3 := range assertTime {
					//The last assertion
					gettingClose := v3.(map[string]interface{})
					tempCode.Name = gettingClose["name"].(string)
					tempCode.Alpha2_code = gettingClose["alpha2_code"].(string)
					tempCode.Alpha3_code = gettingClose["alpha3_code"].(string)
					countryCodes = append(countryCodes, tempCode)
				}
			}
		}
	}
	t = resp.Time()
	return t, countryCodes, nil
}
