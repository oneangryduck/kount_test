package kounttest
import "testing"
import "fmt"

func TestBlankInput(t *testing.T) {
    _, _, err := GetCountryCodes("")
    //Assertion to check for proper error type
    err2 := err.(ErrorBlankLookup) 
    fmt.Println(err2)
}

func TestValidInput(t *testing.T) {
    webtime, body, err := GetCountryCodes("US")
    if err != nil {
	t.Errorf("No error should have happened here")
	t.Fail()
    }
    for i:= range body {
        fmt.Println(body[i])
    }
    fmt.Println(webtime)
}

func TestNoCountryFoundError(t *testing.T) {
    _, _, err := GetCountryCodes("asfasdf")
    //Assertion to check for proper error type
    err2 := err.(NoCountryFoundError) 
    fmt.Println(err2)
}

func TestHTTPServerError(t *testing.T) {
    _, _, err := GetCountryCodes("\xbd\xb2\x3d\xbc\x20\xe2\x8c\x98")
    //Assertion to check for proper error type
    err2 := err.(HTTPServerError) 
    fmt.Println(err2)
}   

func BenchmarkBigCountryRetrieval(b *testing.B) {
    _, _, _ = GetCountryCodes("U")
}

func BenchmarkOneCountryRetrieval(b *testing.B) {
    _, _, _ = GetCountryCodes("USA")
}

func BenchmarkServerError(b *testing.B) {
    _, _, _ = GetCountryCodes("\xbd\xb2\x3d\xbc\x20\xe2\x8c\x98")
}